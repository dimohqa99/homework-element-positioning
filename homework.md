# Размещение элементов на странице [Домашнее задание]

## Задание 1

### Подсчитайте ширину, занимаемую каждым из блоков:

#### 1. <img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.1.01.png" alt="Первый блок">

> Ответ: [ ]px

#### 2.<img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.1.02.png" alt="Первый блок">

> Ответ: [ ]px

#### 3.<img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.1.03.png" alt="Первый блок">

> Ответ: [ ]px

#### 4.<img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.1.04.png" alt="Первый блок">

> Ответ: [ ]px

#### 5.<img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.1.05.png" alt="Первый блок">

> Ответ: []px

## Задание 2

В файле task2.html, вам нужно сверстать форму, элементы которой занимают всю доступную ширину не зависимо от размеров экрана.

Для выполнения этой задачи вам понадобятся дополнительные теги:

- http://htmlbook.ru/html/input
- http://htmlbook.ru/html/select
- http://htmlbook.ru/html/textarea
- http://htmlbook.ru/css/border-spacing

Вся стилизация элементов форм сводится к добавлению границ цвета indigo и полей.

<img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.2.1.png" alt="Задача на display">

<img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.2.2.png" alt="Задача на display">

> _Совет: подумайте, как у элементов форм добиться поведения блочных элементов, чтобы они занимали всю доступную ширину._

## Задание 3

В файле task3.html, cверстайте блоки, иллюстрирующем работу z-index (цвет и прозрачность фона можно не повторять, выберите любые цвета). Измените стили так, чтобы элементы расположились по оси z как на изображении (DIV #4 был выше DIV #1, а DIV #5 — ниже DIV #3):

<img src="https://ulearn.me/Courses/Frontend.Novosibirsk/U04_box_model/./homeTask.3.0.png" alt="Задача на display">

Добейтесь результата изменив только стили, не меняя разметки.
Напишите комментарии в коде стилей, что именно вы изменили и почему.

> _Для прозрачности фона используется `rgba` запись цвета (http://htmlbook.ru/css/value/color#rgba)._